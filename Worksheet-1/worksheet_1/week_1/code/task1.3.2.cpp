#include<iostream>
using namespace std;
int main(){
	int m,n;
	cout << "Enter the value of m:";
	cin >> m;
	cout << "Enter the value of n:";
	cin >> n;
	
	int sum = m+n;
	int difference_mn = m-n;
	int difference_nm = n-m;
	int product = m*n;
	int quotient_mn = m/n;
	int quotient_nm = n/m;
	int modulus_mn = m%n;
	int modulus_nm = n%m;
	
	cout << m <<"+" << n << "=" <<sum <<endl;
	cout << m <<"-" << n << "=" <<difference_mn <<endl;
	cout << n <<"-" << m << "=" <<difference_nm <<endl;
	cout << m <<"*" << n << "=" <<product <<endl;
	cout << m <<"/" << n << "=" <<quotient_mn <<endl;
	cout << n <<"/" << m << "=" <<quotient_nm <<endl;
	cout << m <<"%" << n << "=" <<modulus_mn <<endl;
	cout << n <<"%" << m << "=" <<modulus_nm <<endl;
	
	return 0;	
}