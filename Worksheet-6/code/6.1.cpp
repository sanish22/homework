#include <iostream>
#include <list>
#include<algorithm>
using namespace std;

int main()
{
    list<int> L = {9,8,7,6,5,4,3,2,1,0};
    reverse(L.begin(), L.end());
    for (int x : L) {
        cout << x << " ";
    }
    return 0;
}
