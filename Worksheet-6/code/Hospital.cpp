#include <iostream>
#include <queue>
#include <map>
#include <string>

using namespace std;

class Patient {
    public:
        int patientid;
        int social_id;
        string occupation;
        string disease;
        string treatment;
        bool previous_treated;
        int token_number;
        string dateofVisit;
};

map <string, queue<Patient>> departmentQueue;
map <string, int> patientsPerDay;

void addPatient(Patient p, string department) {
    departmentQueue[department].push(p);
    p.token_number = departmentQueue[department].size();
    patientsPerDay[p.dateofVisit]++;
}

int getTotalPatients() {
    int total = 0;
    for (auto& d : departmentQueue) {
        total += d.second.size();
    }
    return total;
}

int getPatientsPerDay(string date) {
    return patientsPerDay[date];
}

int main(){
    Patient p1;
    p1.patientid = 1;
    p1.social_id = 123;
    p1.occupation = "Student";
    p1.disease = "Flu";
    p1.treatment = "Medication";
    p1.previous_treated = false;
    p1.dateofVisit = "2023-01-20";
    addPatient(p1, "General");

    Patient p2;
    p2.patientid = 2;
    p2.social_id = 987;
    p2.occupation = "Teacher";
    p2.disease = "Cancer";
    p2.treatment = "Surgery";
    p2.previous_treated = true;
    p2.dateofVisit = "2023-01-20";
    addPatient(p2, "Oncology");

    cout << "Total Patients: " << getTotalPatients() << endl;
    cout << "Patients on 2023-01-20: " << getPatientsPerDay("2023-01-20") << endl;

    return 0;
}
