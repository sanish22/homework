#include <iostream>
#include<stack>
using namespace std;
int main(){
    int decimal= 9;
    stack<int>binaryStack;

    while(decimal>0){
        binaryStack.push(decimal % 2);
        decimal /=2;
    }

    while(!binaryStack.empty()){
        cout<<binaryStack.top();
        binaryStack.pop();
    }
    return 0;
}
