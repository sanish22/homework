#include<iostream>
#include<queue>
using namespace std;
int main(){
    queue<int> myQueue;

    //Insert the elements in queue
    myQueue.push(7);
    myQueue.push(8);
    myQueue.push(9);

    //Print the front and back elements
    cout<<"Front element:"<<myQueue.front()<<endl;
    cout<<"Back element:" <<myQueue.back()<<endl;

    //Remove the front element
    myQueue.pop();

    //Print the new front element
    cout<<"New front element:"<<myQueue.front()<<endl;

    return 0;
}
