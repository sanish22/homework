#include<iostream>
using namespace std; 

class Salesperson;
class Sale{
	string dayMonth;
	double saleAmount;
	int salespersonId;
	
	public:
		Sale(string day, double amt, int id){
			dayMonth= day;
			saleAmount = amt;
			salespersonId = id;	
		}
		
	friend void display(Sale, Salesperson);
		
};

class Salesperson{
	int salespersonId;
	string lName;
	
	public:
		Salesperson(int id,string name){
			salespersonId = id;
			lName = name;
		}
		
	friend void display(Sale, Salesperson);
	
};
void display(Sale x, Salesperson y ){
	cout<<"Sale Day of Month:"<<x.dayMonth<<endl;
	cout <<"Amount of the sale:"<<x.saleAmount<<endl;
	cout <<"Salesperson's Id:" <<x.salespersonId<<endl;
	cout<<"Last name of salesperson:"<<y.lName<<endl;
}

int main(){
	Salesperson y(1,"Reus");
	Sale x("July 5",1900,1);
	display(x,y);
	return 0;
}