#include<iostream>
using namespace std; 

class Salesperson;
class Sale{
    string dayMonth;
    double saleAmount;
    int salespersonId;

public:
    Sale(string day, double amt, int id){
        dayMonth= day;
        saleAmount = amt;
        salespersonId = id;    
    }

    int getSalespersonId(){
        return salespersonId;
    }

    friend void display(Sale, Salesperson);
        
};

class Salesperson{
    int salespersonId;
    string lName;

public:
    Salesperson(int id,string name){
        salespersonId = id;
        lName = name;
    }

    int getSalespersonId(){
        return salespersonId;
    }
    friend void display(Sale, Salesperson);

};
void display(Sale x, Salesperson y ){
    cout<<"Sale Day of Month:"<<x.dayMonth<<endl;
    cout <<"Amount of the sale:"<<x.saleAmount<<endl;
    cout <<"Salesperson's Id:" <<x.salespersonId<<endl;
    cout<<"Last name of salesperson:"<<y.lName<<endl;
}

int main(){
    Salesperson spArray[5] = {
        Salesperson(1, "Reus"),
        Salesperson(2, "Messi"),
        Salesperson(3, "Suarez"),
        Salesperson(4, "Pique"),
        Salesperson(5, "Busquets")
    };
    string dayMonth;
    double saleAmount;
    int salespersonId, validId = 0;
    cout<<".........................Fill the sale details below...................................";
    cout << endl;
    
    
    while(true){
    	
        cout<<"Enter Sale Day of Month: (-1 to exit) ";
        cin>>dayMonth;
        if (dayMonth == "-1")break;

        cout<<"Enter Sale Amount: ";
        cin>> saleAmount;
       
        cout<< "Enter SalespersonId:";
        cin>>salespersonId;

     	 for (int i = 0; i < 5; i++){
            if (spArray[i].getSalespersonId() == salespersonId){
                validId = 1;
                break;
            }
        }

        if (validId == 1){
            Sale x(dayMonth, saleAmount, salespersonId);
            for (int i = 0; i < 5; i++){
                if (spArray[i].getSalespersonId() == salespersonId){
                    display(x, spArray[i]);
                    break;
                }
            }
        }
        else{
            cout<<"Invalid Salesperson ID"<<endl;
        }
        validId = 0;
    }
    return 0;
}
