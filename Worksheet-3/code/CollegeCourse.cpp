#include<iostream>
using namespace std;

class Instructor{
	string fName;
	string lName;
	int officeNum;
	
	public:
	Instructor(string fn,string ln, int office){
		fName = fn;
		lName = ln;
		officeNum = office;	
	}
	void display(){
		cout <<"Instructor:"<<fName <<" " <<lName<<endl;
		cout <<"Office:"<<officeNum <<endl;
	}
};

class Classroom{
	string building;
	int roomNum;
	
	public:
		Classroom(string bld, int room){
			building= bld;
			roomNum = room;
		}
		
	void display(){
		cout<<"Building:"<< building<<endl;
		cout <<"Room:"<< roomNum<<endl;
	}
};

class CollegeCourse{
	Instructor prof;
	Classroom place;
	int credits;
	
	public:
		CollegeCourse(string fn, string ln, int office, string bld, int room, int cr):prof(fn,ln,office),place(bld,room){

		prof = Instructor(fn,ln,office);
		place = Classroom(bld,room);
		credits = cr;
		}
	void display(){
		prof.display();
		place.display();
		cout<<"Credits:"<<credits<<endl;
	}
};

int main(){
	CollegeCourse OOP("Sukant","Sahu",404,"TBC-F2",3,30);
	CollegeCourse SQL("Surendra","Osti",707,"TBC-F3",17,28);
	OOP.display();
	cout<<endl;
	SQL.display();
	return 0;
}