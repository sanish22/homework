
#include <iostream>
using namespace std;

// Circle class
class Circle
{
private:
    float rad;

public:
    // Constructor
    Circle(float rd) : rad(rd) {}

    // Virtual operator 
    virtual Circle& operator-(int n)
    {
        rad -= n;
        if (rad < 0)
            throw 10;
        return *this;
    }

    void show()
    {
        cout << rad << endl;
    }
};


class Ellipse : public Circle
{
private:
    float axis;

public:
    // Constructor 
    Ellipse(float rd, float ax) : Circle(rd), axis(ax) {}

    // Overridden operator function 
    Ellipse& operator-(int n)
    {
        Circle::operator-(n);
        axis -= n;
        if (axis < 0)
            throw 20;
        return *this;
    }

     
    void show()
    {
        Circle::show();
        cout << "Semi-major axis: " << axis << endl;
    }
};


void f(Circle& c, int n)
{
    Circle tmp = c - n;
    tmp.show();
}

int main()
{
    Circle cir(5);
    Ellipse ell(10, 6);
    Circle& r1 = cir, &r2 = ell;

    try
    {
        f(r1, 3);
        f(r2, 1);
        f(r2, 10);
    }
    catch (int e)
    {
        if (e == 10)
            cout << "Radius cannot be negative." << endl;
        else if (e == 20)
            cout << "Semi-major axis cannot be negative." << endl;
    }
    return 0;
}