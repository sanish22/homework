
#include<iostream>
using  namespace std;
class Student {
private:
    int code;
    int courses;
    int *grd;

public:
    // Constructor
    Student(int cd = 0, int crs = 0) : code(cd), courses(crs) {
        if (cd > 0)
            grd = new int[courses];
    }

    // Destructor
    ~Student() {
        delete[] grd;
    }

    // Overloaded input operator
    friend istream& operator>>(istream& in, Student& s) {
        do {
            cout << "Enter student's code: ";
            in >> s.code;
            cout << "Enter number of courses: ";
            in >> s.courses;
            if (s.courses <= 0) {
                cout << "Wrong Input";
            }
        } while (s.courses <= 0);
        s.grd = new int[s.courses];
        for (int i = 0; i < s.courses; i++) {
            cout << "Enter grade for course " << i + 1 << ": ";
            in >> s.grd[i];
        }
        return in;
    }

    // Overloaded greater-than operator
    friend bool operator>(const Student& s1, const Student& s2) {
        int passedCourses1 = 0;
                int passedCourses2 = 0;
        for (int i = 0; i < s1.courses; i++) {
            if (s1.grd[i] >= 5) {
                passedCourses1++;
            }
        }
        for (int i = 0; i < s2.courses; i++) {
            if (s2.grd[i] >= 5) {
                passedCourses2++;
            }
        }
        return passedCourses1 > passedCourses2;
    }
    // Copy constructor
    Student(const Student& s) = delete;

    int getCode() {
        return code;
    }
};

int main() {
    int i;
    Student s1, s2;
    cin >> s1 >> s2;
    if (s1 > s2) {
        i = 1;    
    } else if (s2 > s1) {
        i = 2;
    } else {
        i = 3;
       
    }

    if(i==1){
        cout << "Student " << s1.getCode() << " has succeeded in more courses." << endl;
    }
    else if(i==2){
        cout << "Student " << s2.getCode() << " has succeeded in more courses." << endl;
    }
    else if(i==3){
        cout << "Both students have succeeded in the same number of courses." << endl;
    }
    else{
        cout<< "Error";
    }
    return 0;
}