#include<iostream>
using namespace std;
class School
{
private:
    string name;

public:
    School(const string& n) : name(n) {}
    virtual ~School() {}
    virtual void show() const = 0;
};

class Programming : virtual public School
{
private:
    int courseNo;

public:
    Programming(const string& n, int num) : School(n), courseNo(num) {}
    virtual void show() const;
};

class Network : virtual public School
{
private:
    int courseNo;

public:
    Network(const string& n, int num) : School(n), courseNo(num) {}
    virtual void show() const;
};

class Student : public Programming, public Network
{
private:
    string stdname;
    int code;

public:
    Student(const string& schoolName, const string& stdName, int stdCode, int numProgC, int numNetC) :
        School(schoolName), Programming(schoolName, numProgC), Network(schoolName, numNetC), stdname(stdName), code(stdCode) {}
    void show() const;
};

void School::show() const
{
    cout << "School's Name: " << name << endl;
}

void Programming::show() const
{
    cout << "Courses in Programming: " << courseNo << endl;
}

void Network::show() const
{
    cout << "Courses in Network: " <<courseNo << endl;
}

void Student::show() const
{
    School::show();
    Programming::show();
    Network::show();
    cout << "Name of the student: " << stdname << endl;
    cout << "Student's Code: " << code << endl;
}

int main()
{
 Student s("TBC", "Prabhat", 100, 10, 12); 
 School& p = s;
 p.show(); 
 return 0;
}

