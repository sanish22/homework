#include <iostream>
#include <fstream>

using namespace std;

struct Employee{
char name[100];
int taxno;
double salary;
};

int main(){
struct Employee employees[100];
int count = 0;
for (count=0; count<100; count++) {
    cout << "Enter employee name(or 'fin' to exit):";
    cin >> employees[count].name;
    
    if (strcmp(employees[count].name, "fin") == 0){
        break;
    } 
    

    cout << "Enter employee's tax number:";
    cin >> employees[count].taxno;
    
    cout << "Enter employee's salary:";
    cin >> employees[count].salary;
}

ofstream outfile;
outfile.open("test.bin", ios::binary); //Open the binary file
outfile.write((char *)&employees, sizeof(struct Employee) * count); //Write the value of employees to file
outfile.close(); //Close the file
return 0;
}
