#include <iostream>
#include <fstream>
using namespace std;

struct Employee {
    char name[100];
    int taxno;
    double salary;
};

int main() {
    ifstream infile("test.bin", ios::binary); //Open test.bin file in binary mode
    int number_of_employees = 0;

    Employee employees[100];
    //Read employees data from test.bin
    while (infile.read((char*)&employees[number_of_employees], sizeof(Employee))) {
        number_of_employees++;
    }
    infile.close(); //Close test.bin file

    double salary_threshold;
    cout << "Enter the minimum salary: ";
    cin >> salary_threshold;

    int count = 0;
    Employee employees_data[number_of_employees];
    double total_salary = 0;

    for (int i = 0; i < number_of_employees; i++) {
        if (employees[i].salary > salary_threshold) {
            employees_data[count] = employees[i];
            total_salary += employees[i].salary;
            count++;
        }
    }

    ofstream outfile("data.bin", ios::binary); //Open the data.bin file in binary
    outfile.write((char*)employees_data, sizeof(Employee) *count); //Write the employees data to the file
    outfile.close();//Close the data.bin file

    double average_salary = total_salary / count;
    cout << "The average salary of the employees in data.bin is: " << average_salary << endl;
    return 0;
}
